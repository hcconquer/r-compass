$(function() {

console.log('options start');

function initPage() {
	chrome.storage.sync.get(null, function(item) {
		$('#account').val(item['account']);
		$('#passwd').val(item['passwd']);
		$('#auto_login').prop('checked', item['auto_login']);
		$('#check_period').val(item['check_period']);
	});	
}

$('#saveopt').on('click', function() {
	chrome.storage.sync.set({
		'account': $('#account').val(),
		'passwd': $('#passwd').val(),
		'auto_login': $('#auto_login').prop('checked'),
		'check_period': $('#check_period').val()
	}, function() {
	});
});

$('#login').on('click', function() {
	chrome.storage.sync.set({
		"auto_login": true
	}, function() {
		initPage();
		chrome.extension.sendRequest( 
			{ 
				"action": "rcompass_login"
			}, 
			function(response) {
				console.debug(JSON.stringify(response));
			}
		);
	});
});

$('#logout').on('click', function() {
	chrome.storage.sync.set({
		"auto_login": false
	}, function() {
		initPage();
		chrome.extension.sendRequest( 
			{ 
				"action": "rcompass_logout"
			}, 
			function(response) {
				console.debug(JSON.stringify(response));
			}
		);
	});
});

var manifest = chrome.runtime.getManifest();
$('#version').text(manifest.version);
initPage();

});

