$(function() {

console.log("login.js start");

function getCurrentView() {
	var url = window.location.href;
	console.debug("current_url: %s", JSON.stringify(url));
	if (url.indexOf("chrome-extension") >= 0) {
		return "popup";
	}
	return "content";
}

function initPage() {
	chrome.storage.sync.get(null, function(item) {
		// $("#login-form #email").val(item['account']);
		// $("#login-form #password").val(item['passwd']);
		// $("#login-form #remeberMe").prop("checked", item['remeber_account']);
		// $('#auto_login').prop('checked', item['auto_login']);
		// $('#check_period').val(item['check_period']);
	});	
}

$("#login-form").submit(function() {
	console.debug("login");
	var account = $("#login-form #email").val();
	var passwd = $("#login-form #password").val();
	var remeber_account = $("#login-form #remeberMe").is(':checked'); //  ? 'remember' : '');
	$("#login-box .alert-danger").removeClass("show").addClass("hidden");

	chrome.storage.sync.set({
		"account": account,
		"passwd": passwd,
		"auto_login": true,
		"remeber_account": remeber_account
	}, function() {
		chrome.extension.sendRequest( 
			{ 
				"action": "rcompass_login"
			}, 
			function(response) {
				console.debug(JSON.stringify(response));
				if (response.result == 0) {
					var view = getCurrentView();
					if (view == "popup") {
						window.close();	
					}
				} else {
					$("#login-box .defaultLoginAlert").removeClass("hidden").addClass("show");
				}
			}
		);
	});
	return false;
});

var manifest = chrome.runtime.getManifest();
$('#version').text(manifest.version);
initPage();

getCurrentView();

});

