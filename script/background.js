(function(window) {

console.log("background.js start");

var CTXT_MENU_LOGOUT = "ctxt_menu_logout";

function initCtxt(params) {
	var logout_title = chrome.i18n.getMessage("logout_title");
	if ((params) && (!$.isEmptyObject(params))) {
		if ($.isFunction(params["logout"])) {
			chrome.contextMenus.create({
				"id": CTXT_MENU_LOGOUT,
				"title": logout_title,
				"contexts": [ "browser_action" ],
				"onclick": params["logout"]
			});
		}
	}
}

/*
 * used for set popup, title, menu e.g.
 */
function setCtxt(params) {
	console.debug('set ctxt, param: %s', JSON.stringify(params));
	var title = "", badge = "", popup = "login.html", logout = false;
	var manifest = chrome.runtime.getManifest();
	title = chrome.i18n.getMessage('title_version', [ manifest.version ]);
	if ((params) && (!$.isEmptyObject(params))) {
		if (params['active_alert_count']) {
			var active_alert_count = params['active_alert_count'];
			if (active_alert_count >= 0) {
				title = chrome.i18n.getMessage('title_alert', [ active_alert_count ]);
				var bdg = strfmt(active_alert_count, 4, (4 - active_alert_count.length) / 2);
				badge = chrome.i18n.getMessage('badge_alert', [ bdg ]);
				popup = "";
				logout = true;
			}
		}
	}
	chrome.browserAction.setTitle({
		"title": title
	});
	chrome.browserAction.setBadgeText({
		"text": badge
	});
	chrome.browserAction.setPopup({
		"popup": popup
	});
	chrome.contextMenus.update(CTXT_MENU_LOGOUT, {
		"enabled": logout
	}, function() {
	});
}

function reloadPage(params) {
	var url = "<all_urls>";
	if ((params) && (params.url)) {
		url = params.url;
	}
	console.debug("url: %s", url);
	chrome.tabs.query({
		"url": url
	}, function(tabs) {
		for (var i = 0; i < tabs.length; i++) {
			// console.debug("%s", JSON.stringify(tab));
			var tab = tabs[i];
			chrome.tabs.reload(tab.id, {}, function() {
			});	
		}
	});
}

/*
 * check is the any cmcc's bu can auto jump to main page from index/login
 */
function pageOpenListener(tab) {
}

/*
 * when page is all loaded include css and images
 */
function pageReadyListener(tab) {
	// console.debug(tab.url);
}

function tabCreatedListener(tab) {
	// console.debug(tab.url);
}

function tabUpdatedListener(tabId, event, tab) {
	// console.debug("%s %s %s", tabId, JSON.stringify(event), JSON.stringify(tab));
	if (event.status) {
		if (event.status == 'loading') {
			pageOpenListener(tab);
		} else if (event.status == 'complete') {
			pageReadyListener(tab);
		}
	}
}

function tabRemovedListener(tabId, info) {
}

function beforeRequestListener(details) {
	var url = details.url;
	var idx = -1;
	
	/*
	idx = url.indexOf("login.php");
	if (idx >= 0) {
		console.debug("%s", JSON.stringify(details));
		console.debug("%s", JSON.stringify(details.requestBody));
	}
	*/
	
	idx = url.indexOf("logout.php");
	if (idx >= 0) {
		chrome.storage.sync.set({
			"auto_login": false
		}, function() {
			setCtxt({
				"active_alert_count": -1
			});
		});
	}
	
	if (details.tabId < 0) {		
		idx = url.indexOf("/assets");
		if (idx >= 0) {
			var nurl = "http://tlm.corrsight.com/rcompass" + url.substr(idx);
			return { redirectUrl: nurl };
		}
	}
}

function completedRequestListener(details) {
}

function popupClickedListener(tab) {
	console.debug("%s", JSON.stringify(tab));
	chrome.tabs.query({
		"url": "http://tlm.corrsight.com/rcompass/alerts_page.php"
	}, function(tabs) {
		if (tabs.length > 0) { // cmpass tab opened
			var dtab = tabs[0];
			chrome.tabs.query({
				"active": true
			}, function(tabs) {
				var curtab = tabs[0];
				if (curtab.id == dtab.id) {
					// showMainPage(curtab);
				} else {
					chrome.tabs.update(dtab.id, {
						"active": true
					}, function(umctab) {
					});
				}
			});
		} else { // tab not open
			chrome.tabs.create({
				"url": "http://tlm.corrsight.com/rcompass/alerts_page.php"
			}, function(tab) {
			});
		}
	});
}

/*
 * get request from page or popup
 */
function extensionRequestListener(request, sender, sendResponse) {
	console.debug(JSON.stringify(request));
	var resp = { 
		"action": request.action,
		"result": 0
	};
	if (request.action == 'rcompass_login') {
		rcompassLogin({
			"success": function() {
				sendResponse(resp);
				rcompassCheckAlerts({
					"success": setCtxt
				});
			},
			"failure": function() {
				resp["result"] = 1;
				resp["error"] = "login_fail";
				sendResponse(resp);
			}
		});
		// rcompassCheckAlerts(updateView, null);
	} else if (request.action == 'rcompass_logout') {
		rcompassLogout({
			"success": function() {
				setCtxt({
					"active_alert_count": -1
				});
			}
		});
		/*
		setCtxt({
			"active_alert_count": -1
		});
		*/
	} else if (request.action == 'rcompass_check_alerts') {
		rcompassCheckAlerts({
			"success": setCtxt
		});
	} else {
		resp["result"] = 1;
		resp["error"] = "unknown action";
	}
}

function onInstalledListener(info) {
	console.debug(JSON.stringify(info));
	// extension install or update
	if (info.reason == 'install'){
		saveOptions(null); 
		// showOptionsPage(null);
	} else if (info.reason == 'update') {
		// showOptionsPage(null);
	}
}

function work() {
	var last_check_time = 0, now_time;
	
	setInterval(function() {
		now_time = Math.round(new Date().getTime() / 1000);
		
		chrome.storage.sync.get(null, function(item) {
			// console.debug("auto_login: %s", item['auto_login']);
			if (item['auto_login']) {
				var check_period = 1;
				if (item['check_period']) {
					check_period = item['check_period'];
				}
				// console.debug("check_period: %s", check_period);
				
				if (now_time - last_check_time >= check_period) {
					rcompassCheckAlerts({
						"success": setCtxt, 
						"error": rcompassLogin
					});
					last_check_time = now_time;
				} else {
					// console.debug("not enough check time, %s, %s, %s", now_time, last_check_time, check_period);
				}
			} else {
				// rcompass_logout(null);
			}
		});
	}, 500);
}

var manifest = chrome.runtime.getManifest();
console.debug(JSON.stringify(manifest));

chrome.webRequest.onBeforeRequest.addListener(
		beforeRequestListener, 
		{ urls: [ "<all_urls>" ] },
		[ "blocking", "requestBody" ]
);

chrome.webRequest.onCompleted.addListener(
		completedRequestListener,
		{ urls: [ "<all_urls>" ] }
);

chrome.tabs.onCreated.addListener(tabCreatedListener);
chrome.tabs.onUpdated.addListener(tabUpdatedListener);
chrome.tabs.onRemoved.addListener(tabRemovedListener);

chrome.browserAction.onClicked.addListener(popupClickedListener);
chrome.extension.onRequest.addListener(extensionRequestListener);
chrome.runtime.onInstalled.addListener(onInstalledListener);

initCtxt({
	"logout": function() {
		rcompassLogout({
			"success": function() {
				setCtxt({
					"active_alert_count": -1
				});
				reloadPage({
					"url": "*://*.corrsight.com/*"
				});
			}
		});
	}
});
setCtxt({});
work();

})(window);
