function strfmt(str, len, right) {
	var left = len - str.length - right;
	for (var i = 0; i < left; i++) {
		str = " " + str;
	}
	for (var i = 0; i < right; i++) {
		str = str + " ";
	}
	return str;
}

function getDomainByUrl(url) {
	var match = url.match(/:\/\/(.[^/]+)/);
	if (typeof match == 'undefined' || match == null) {
		return null;
	}
	return match[1];
}

function getMainDomainByDomain(domain) {
	var ds = domain.split('.');
	var num = ds.length;
	if (num >= 3) {
		if (ds[num - 2] == 'com') {
			return ds[num - 3] + '.' + ds[num - 2] + '.' + ds[num - 1];
		} else {
			return ds[num - 2] + '.' + ds[num - 1];
		}
	} else {
		return domain;
	}
}

function getMainDomainByUrl(url) {
	var domain = getDomainByUrl(url);
	return getMainDomainByDomain(domain);
}

function getProtocolByUrl(url) {
	var idx = url.indexOf(':');
	if (idx < 0) {
		return null;
	}
	return url.substring(0, idx);
}

function addScript(script) {
	$('<script></script>').attr({type: 'text/javascript'})
	.text(script).appendTo($('body'));
}

function showOptionsPage(tab) {
	var manifest = chrome.runtime.getManifest();
	var url = chrome.runtime.getURL(manifest.options_page);
	if (tab) {
		chrome.tabs.update(tab.id, {
			"url": url
		});
	} else {
		chrome.tabs.create(
			{
				"url": url
			}, 
			function(ntab) {
			}
		);
	}
}

/*
function checkShowOptionsPage() {
	var manifest = chrome.runtime.getManifest();
	chrome.storage.sync.get(['version'], function(item) {
		var chg = 0; // not change
		if (item['version']) {
			if (manifest.version > item['version']) {
				// console.debug('update');
				chg = 1; // update
			} else if (manifest.version < item['version']) {
				chg = -1;
			}
		} else {
			// console.debug('install');
			chg = 2; // full new install
			saveOptions(null); // use default
		}
		console.debug("oldver: %s, newver: %s, chg: %d", item['version'], manifest.version, chg);
		if (chg != 0) {
			var url = chrome.runtime.getURL(manifest.options_page);
			chrome.tabs.create(
				{
					"url": url
				}, 
				function(tab) {
				}
			);
			chrome.storage.sync.set(
				{
					'version': manifest.version
				}, 
				function() {
				}
			);
		}
	});
}
*/
