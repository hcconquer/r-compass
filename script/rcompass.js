function saveOptions(options) {
	var defopts = {
		auto_login: true,
		check_period: 120
	};
	var opts = options || defopts;
	chrome.storage.sync.set({
		account: opts['account'], // lev1@test.com
		passwd: opts['passwd'],
		auto_login: opts['auto_login'],
		check_period: opts['check_period']
	}, function() {
	});
}

function rcompassLogin(params) {
	var prms = params || {};
	var done = false;
	console.debug("rcompass_login, %s", JSON.stringify(params));
	if ((params) && (!$.isEmptyObject(params))) {
		if (params['account']) {
			$.ajax("http://tlm.corrsight.com/rcompass/ajax/login.php", {
				type: "POST",
				data: {
					action: "login",
					email: params['account'],
					password: params['passwd']
				},
				success: function(data, textStatus, jqXHR) {
					console.debug("login response, %s", JSON.stringify(data));
					if (data) {
						// console.debug("login success");
						if ($.isFunction(params["success"])) {
							params.success();
						}
					} else {
						// console.debug("login fail");
						if ($.isFunction(params["failure"])) {
							params.failure();
						}
					}
				}
			});
			done = true;
		}
	} 
	if (!done) {
		chrome.storage.sync.get(null, function(item) {
			prms["account"] = item["account"];
			prms["passwd"] = item["passwd"];
			rcompassLogin(prms);
		});
	}
}

function rcompassLogout(params) {
	console.debug("rcompass_logout");
	$.ajax("http://tlm.corrsight.com/rcompass/logout.php", {
		type: "GET",
		success: function(data, textStatus, jqXHR) {
			// console.debug("logout response, %s", JSON.stringify(data));
			if ($.isFunction(params["success"])) {
				params.success();
			}
		}
	});
}

function rcompassCheckAlerts(params) {
	console.debug("rcompass_check_alerts");
	$.ajax("http://tlm.corrsight.com/rcompass/alerts_stats.php", {
		type: "GET",
		success: function(data, textStatus, jqXHR) {
			var jpage = $(data);
			var alert_rows = $(".profile-info-row", jpage).filter(function(index) {
				var row = $(this);
				var name = $(".profile-info-name", row);
				if (name.text().indexOf("Unhandled") >= 0) {
					return true;
				}
			});
			if (alert_rows.length == 1) {
				var active_alert = $(".profile-info-value", alert_rows.get(0));
				var active_alert_count = $.trim(active_alert.text());
				console.debug("alert_count: %s", active_alert_count);
				if ($.isFunction(params["success"])) {
					params.success({
						"active_alert_count": active_alert_count
					});
				}
			} else {
				if ($.isFunction(params["error"])) {
					params.error(null);
				}
			}
		}
	});
}